function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}

function getListOfRandomNumbers(min, max, length = 5) {

    let list = [];

    for (let i = 0; i < length; i++) {

        let number = getRandomInt(min, max);

        // make sure the next number is NOT already in the row
        while(list.includes(number)) {
            number = getRandomInt(min, max)
        }

        list.push(number);
    }

    return list;

}

export function getNumbers() { 
    return Array.from(Array(75).keys()).map(number => number + 1); 
}

export function createBingoCard75() {
    let card = [];

    let b = getListOfRandomNumbers(1, 15);
    let i = getListOfRandomNumbers(16, 30);
    let n = getListOfRandomNumbers(31, 45);
    let g = getListOfRandomNumbers(46, 60);
    let o = getListOfRandomNumbers(61, 75);


    // This `turns` the array so that the 2d array resembles the bingo card
    for (let index = 0; index < 5; index++) {
        let row = [b[index], i[index], n[index], g[index], o[index]];
        card.push(row);
    }
    
    // N row middle spot gets replaced with `undefined` here, this is the 'free' spot
    card[2][2] = undefined;

    return card;
}