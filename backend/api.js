"use strict";

import {webSocketServer} from './index.js';
import { createBingoCard75, getNumbers } from './bingocard.js';
import { addGame, listOfGames, setWinnerGame, getJoinableGames, startGame, joinGame, addUser } from './database.js';
import {Router} from 'express';

export const router = Router();
const countdownInSeconds = 8;

let connectionCount = 0;

router.get('/card', function (req, res) {
    res.json(createBingoCard75());
});

router.get('/game', function (req, res) {
    res.json(listOfGames());
});

router.get('/leaderboard', function(req, res) {
    res.json(getLeaderBoard())
}) 

router.post('/game', function (req, res) {
    const name = req.body.name;
    const result = addGame(name);

    res.json(result);
});

router.put('/game/winner', function (req, res) {
    const game = req.body.game;
    const winner = req.body.winner;
    const result = setWinnerGame(game, winner);

    res.json(result);

});

router.put('/game/join', function (req, res) {
    const gameName = req.body.game;
    const userName = req.body.user;

    // add user if not exists yet
    addUser(userName);
    const result = joinGame(gameName, userName);

    res.json(result);
});


router.get('/game/joinable', function (req, res) {
    res.json(getJoinableGames())
});


function createGame() {
    const gameName = genKey();

    addGame(gameName);

    return gameName;
}


function genKey(length = 6) {
    // create an random string of x amount of characters for the room key
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
    for (let i = 0; i < length; i++) {
        result += characters.charAt(
          Math.floor(Math.random() * characters.length));
    }
    return result;
}

// Handle WebSocket connections
webSocketServer.on('connection', (webSocket, req) => {
    // Assign an id to keep track of WebSockets for logging purposes
    webSocket.id = ++connectionCount;

    console.log('Incoming WebSocket connection', webSocket.id);
    webSocket.on('message', (data) => {
        data = JSON.parse(data);
        console.log('Incoming WebSocket data', data, 'from', webSocket.id, webSocket.room);
    
        switch (data.command) {
            case 'createRoom':
                let roomKey = createGame();
        
                webSocket.id = roomKey;
                webSocket.numbers = getNumbers();
                webSocket.send(JSON.stringify({command: 'room', key: roomKey}));
                break;

            case 'startGame':
                startGame(webSocket.id);                
                function newNum() {
                    if (webSocket.numbers.length == 0) {
                        clearInterval();
                    }

                    let index = Math.floor(Math.random() * webSocket.numbers.length)
                    let chosenNumber = webSocket.numbers[index];
                
                    webSocket.numbers.splice(index, 1)

                    webSocket.send(JSON.stringify({command:'newNumber', number: chosenNumber }));
                }

                newNum();

                setInterval(() => {
                    newNum();
                }, 1000 * countdownInSeconds);
                break;

            case 'join':
                let joinable = false;
                for (const client of webSocketServer.clients) {
                    if (client.id == data.key)
                    {
                        joinable = true;
                    }
                }

                if (joinable) {
                    webSocket.id = data.name;
                    webSocket.room = data.key;
                    webSocket.send(JSON.stringify({command:'joined'}));
                }
                break;

            case 'checkWin':
                const numbers = data.numbers;
                let won = true;

                // 5x5 contains 24 valid spots, excluding free. 
                if (numbers.length != 24) {
                    won = false;
                }

                // this is for an full card
                for (const client of webSocketServer.clients) {
                    if (client.id == webSocket.room) {
                        for (const number of numbers) {
                            if (client.numbers.includes(number)) {
                                won = false;
                            }
                        }
                    }
                }
                
                if (won) {
                    sendMessage(webSocket, `${webSocket.id} has won!`);
                    // setWinnerGame(webSocket.room, webSocket.id);
                }
                else {
                    sendMessage(webSocket, `${webSocket.id} has fake bingo. Boo!`);
                }
                break;

            case 'numberState':
                if (data.state == 'crossed') {
                    webSocketServer.clients.forEach(function each(client) {
                        if (client.id == webSocket.room) {
                            // send a message to the room 50% of the time so somebody can actually forget
                            if (client.numbers.includes(data.number) && Math.random() > 0.50) {
                                sendMessage(webSocket, `${webSocket.id} crossed an number that has not been drawn yet`);
                            }
                        }
                    });
                }

                break;

            default:
                console.log("Invalid command", data);
                break;
        }
    });
});


function sendMessage(webSocket, message) {
    webSocketServer.clients.forEach(function each(client) {
        if (client.readyState === client.OPEN && client.id == webSocket.room) {
                client.send(JSON.stringify({ command: 'message', message: message}))
            }
    });
}