import { readFileSync } from 'node:fs';
import Database from 'better-sqlite3';

const db = new Database(process.env.DB || 'sqlite3.db');

export function applySchema() {
	// Make sure tables and initial data exist in the database
	db.exec(readFileSync('schema.sql').toString());
	db.prepare('PRAGMA foreign_keys = ON').run();
}

export function dropAllTables() {
	db.prepare('PRAGMA foreign_keys = OFF').run();
	for(let row of db.prepare("SELECT name FROM sqlite_master WHERE type = 'table' AND name != 'sqlite_sequence'").all()) {
		db.prepare(`drop table "${row['name']}"`).run();
	}
}

export function addGame(name) {
	return db.prepare('INSERT INTO games (name, joinable) VALUES(?,1)').run(name);
}

export function setWinnerGame(game, winner) {
	return db.prepare('UPDATE games SET winner_id = (SELECT id FROM users WHERE name = ?)').run(winner, game);
}

export function startGame(game) {
	return db.prepare('UPDATE games SET joinable = 0 WHERE name = ?').run(game);
}

export function addUser(userName) {
	return db.prepare('INSERT OR IGNORE INTO users(name) VALUES (?)').run(userName)
}

export function joinGame(gameName, userName) {
	return db.prepare('INSERT INTO users_games(game_id, user_id) VALUES((SELECT id FROM games WHERE name = ?), (SELECT id FROM users WHERE name = ?))').run(gameName, userName);
}

export function listOfGames() {
	return db.prepare('SELECT * FROM games').all();
}

export function getJoinableGames() {
	return db.prepare('SELECT * FROM games WHERE joinable = 1 LIMIT 5').all();
}
