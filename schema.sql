create table if not exists users(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT UNIQUE
);

create table if not exists games(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name TEXT,
    joinable BOOLEAN,
    winner_id INTEGER
);

create table if not exists users_games(
    game_id INTEGER,
    user_id INTEGER
)