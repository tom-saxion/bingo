it('Card', () => { 
    cy.request('/api/card').then((response) => {
        expect(response).to.have.property('status', 200)
        expect(response.body).to.not.be.null;
        expect(response.body).to.have.length(5)
        
        let freeSpotCount = 0;

        for (const list of response.body) {
            for (const number of list) {
                if (number) {
                    expect(number).lte(75);
                    expect(number).gt(0);
                }
                else {
                    freeSpotCount++
                }
            }
        }

        expect(freeSpotCount).eq(1);

    })
})