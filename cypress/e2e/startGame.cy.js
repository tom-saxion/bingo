it('Starts an game', () => { 
    cy.visit('/');

    cy.get('input').contains('Create Room').click()

    cy.get('input').click();
    
    cy.get('.inner-circle').invoke('text').then((text) => {
        const value = parseInt(text);
      
        cy.wrap(value).should('be.gte', 1).and('be.lte', 75);
    });
})