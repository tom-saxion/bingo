
const cacheName = 'cache';

self.addEventListener('install', async (event) => {
  event.waitUntil(
    async () => {
      await caches.open(cacheName);
    }
  );
});

self.addEventListener('fetch', async (event) => {
  const cache = await caches.open(cacheName);
  let res = undefined;

  try {
    res = await fetch(event.request);
    await cache.put(event.request, res.clone());
  }
  catch (error) {
    console.log(error)
    res = await cache.match(event.request);
  }

  event.respondWith(res);

});
